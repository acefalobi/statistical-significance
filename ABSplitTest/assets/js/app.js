$(document).ready(function () {
    $('#test-form').submit(function (event) {
        event.preventDefault();

        $('.spinner').addClass('show').removeClass('hide');

        var resultRequest = $.ajax({
            url: '/results/?' + $(this).serialize(),
            method: 'GET'
        });

        resultRequest.done(function (data) {
            var resultsWrapper = $('#results-wrapper');

            resultsWrapper.html(data);
            $('.spinner').addClass('hide').removeClass('show');

            $('html, body').stop().animate({
                scrollTop: resultsWrapper.offset().top
            }, 1000, 'easeInOutExpo');

            event.preventDefault();
        });

        resultRequest.fail(function (error, status) {
            $('.spinner').addClass('hide').removeClass('show');
            alert("Request Failed: " + status);
            console.error(error)
        });

        return false;
    })
});