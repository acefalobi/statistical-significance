from django.apps import AppConfig


class SignificanceCalculatorConfig(AppConfig):
    name = 'significance_calculator'
