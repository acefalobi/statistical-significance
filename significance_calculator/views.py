import math
from django.shortcuts import render


# Create your views here.

def main_page(request):
    return render(request, "main.html")


def results_page(request):
    props = {"is_valid": False}

    if request.method == 'GET':
        if 'c_visitors' in request.GET and 'v_visitors' in request.GET and 'c_conversions' in request.GET and 'v_conversions' in request.GET:
            control_visitors = int(request.GET.get('c_visitors'))
            variation_visitors = int(request.GET.get('v_visitors'))
            control_conversions = int(request.GET.get('c_conversions'))
            variation_conversions = int(request.GET.get('v_conversions'))

            control_conversion_rate = control_conversions / control_visitors
            variation_conversion_rate = variation_conversions / variation_visitors

            standard_error = math.sqrt(
                (
                    (control_conversion_rate * (1 - control_conversion_rate)) / control_visitors
                ) + (
                    (variation_conversion_rate * (1 - variation_conversion_rate)) / variation_visitors
                )
            )

            if variation_conversion_rate - control_conversion_rate != 0:
                z_value = (variation_conversion_rate - control_conversion_rate) / standard_error

                d1 = 0.0498673470
                d2 = 0.0211410061
                d3 = 0.0032776263
                d4 = 0.0000380036
                d5 = 0.0000488906
                d6 = 0.0000053830

                a = math.fabs(z_value)

                p_value = 1 + a * (d1 + a * (d2 + a * (d3 + a * (d4 + a * (d5 + a * d6)))))
                p_value *= p_value
                p_value *= p_value
                p_value *= p_value
                p_value *= p_value
                p_value = 1 / (2 * p_value)

                if z_value >= 0:
                    p_value = 1 - p_value

                if p_value < 0.05:
                    props["is_significant"] = 'Yes'
                else:
                    props["is_significant"] = 'No'

                props["p_value"] = round(p_value, 4)
            else:
                props["is_significant"] = "Not Possible"
                props["p_value"] = "Not Possible"

            props["is_valid"] = True

    return render(request, "results.html", props)
