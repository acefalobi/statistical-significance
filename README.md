# README #

Get this App up and running

### What is this repository for? ###

* This is a web app that allows you to test for statistical significance in your A/B Split Tests
* 1.0

### How do I get set up? ###

* Simply clone the repository
* Install Python 3
* Install Django
* There's no need for DB configuration as this app doesn't make use of one
* Go to the terminal and cd into the repository folder
* Finally run 'python manage.py runserver'

### Who do I talk to? ###

* For more info, email me at: acefalobi@aceinteract.com